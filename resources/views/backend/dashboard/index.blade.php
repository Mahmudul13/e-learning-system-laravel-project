<x-master>
    <x-slot:title>
        Dashboard
        </x-slot>
        <div class="row">
            <div class="col-md-3 rounded shadow m-2 p-4 d-flex"
                style="
            height: 150px;
            width: 250px;
            background: linear-gradient(
            to bottom left,
            #f86e66 0%,
            #fe5195 100%
            );">
                <div class="mt-4">
                    <i class="fa-solid fa-chart-line fs-4 p-2 me-4"
                        style="
                    border-radius: 50%;
                    background-color: #c04235;
                    color: #ffffff;"></i>
                </div>
                <div class="mt-4 text-light">
                    <span class="fw-bolder">97,567</span>
                    <br />
                    <span>Course Revinue</span>
                </div>
            </div>
            <div class="col-md-3 bg-warning rounded shadow m-2 p-4 d-flex"
                style="
            height: 150px;
            width: 250px;
            background: linear-gradient(
            to top right,
            #84f9b2 0%,
            #8fd4f2 100%
            );">
                <div class="mt-4">
                    <i class="fa-solid fa-user fs-4 p-2 me-4"
                        style="
                    border-radius: 50%;
                    background-color: #529c7b;
                    color: #ffffff;"></i>
                </div>
                <div class="mt-4 text-light">
                    <span class="fw-bolder">330</span>
                    <br />
                    <span>New User</span>
                </div>
            </div>
            <div class="col-md-3 bg-warning rounded shadow m-2 p-4 d-flex"
                style="
            height: 150px;
            width: 250px;
            background: linear-gradient(
            to top right,
            #fb8bdc 0%,
            #ad62f8 100%
            );">
                <div class="mt-4">
                    <i class="fa-solid fa-bag-shopping fs-4 p-2 me-4"
                        style="
                    border-radius: 50%;
                    background-color: #a238b9;
                    color: #ffffff;"></i>
                </div>
                <div class="mt-4 text-light">
                    <span class="fw-bolder">45,330</span>
                    <br />
                    <span>Course Sold</span>
                </div>
            </div>
            <div class="col-md-3 rounded shadow m-2 p-4 d-flex"
                style="
            height: 150px;
            width: 250px;
            background: linear-gradient(
            to top right,
            #ff5957 0%,
            #f1951c 100%
            );">
                <div class="mt-4">
                    <i class="fa-solid fa-heart fs-4 p-2 me-4"
                        style="
                    border-radius: 50%;
                    background-color: #B01818;
                    color: #ffffff;"></i>
                </div>
                <div class="mt-4 text-light">
                    <span class="fw-bolder">5,330</span>
                    <br />
                    <span>Happy Student</span>
                </div>
            </div>
        </div>
</x-master>
